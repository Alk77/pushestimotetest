//
//  EstimotionManager.swift
//  PushEstimoteTest
//
//  Created by UDTech LLC on 4/23/19.
//  Copyright © 2019 UDTech LLC. All rights reserved.
//

import UIKit
import EstimoteProximitySDK

enum EstimoteState {
    case notStarted
    case startFromForeground
    case startFromFrequentPlace
    case startFromPush
}

class EstimotionManager: NSObject {

    static let shared = EstimotionManager()
    
    fileprivate var timer: Repeater?
    fileprivate var state: EstimoteState = .notStarted
    fileprivate var proximityObserver: ProximityObserver!
    fileprivate let zone = ProximityZone(tag: "Coworking Space,room", range: ProximityRange(desiredMeanTriggerDistance: 3.0)!)
    
    var isValidTimeToStartTracking: Bool {
        let formatter = DateFormatter()
        formatter.dateFormat = "HH"
        guard let currentHour: Int = Int(formatter.string(from: Date())) else {
            return false
        }
        return currentHour >= 22 ? false : true
    }
    
    private override init() {
        super.init()
        let cloudCredentials = CloudCredentials(appID: "philippe-jemelin-dashcom-c-8b5",
                                                appToken: "8b9b600d30875bf4ae6e3c5909ad7964")
        self.proximityObserver = ProximityObserver(
            credentials: cloudCredentials,
            onError: { error in
                print("proximity observer error: \(error)")
        })
//        setupEstimote()
    }
    
    fileprivate func setupEstimote() {
        zone.onEnter = { [weak self] context in
            let string: String = "Enter: device \(context.deviceIdentifier) | tag: \(context.tag) | Attachments: \(context.attachments)"
            self?.updateLogs(string)
//            self?.stopEstimotion()
            if let url = URL(string: "https://ios-stage.dashwork.ch:1234/api/tracking/enter") {
                let semaphore = DispatchSemaphore(value: 0)
                
                var request = URLRequest(url: url)
                request.httpMethod = "POST"
                request.setValue("application/json", forHTTPHeaderField: "Content-Type")
                request.setValue("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6Miwicm9sZSI6InVzZXIiLCJpYXQiOjE1NDk0NjM1NzR9.X-d7mmVGPBA-_DzL36xBsTMhJP-Q0wId3MUoe_6SNv4", forHTTPHeaderField: "Authorization")
                let params = ["roomId": 2]
                
                guard let httpBody = try? JSONSerialization.data(withJSONObject: params, options: []) else {
                    return
                }
                request.httpBody = httpBody
                
                let session = URLSession.shared
                session.dataTask(with: request, completionHandler: { (data, response, error) in
                    print(String(data: data!, encoding: String.Encoding.utf8)!)
                    semaphore.signal()
                }).resume()
                semaphore.wait()
            }
        }
        zone.onExit = { [weak self] context in
            let string: String = "Exit: device \(context.deviceIdentifier) | tag: \(context.tag) | Attachments: \(context.attachments)"
            self?.updateLogs(string)
//            self?.stopEstimotion()
            
            if let url = URL(string: "https://ios-stage.dashwork.ch:1234/api/tracking/exit") {
                let semaphore = DispatchSemaphore(value: 0)
                
                var request = URLRequest(url: url)
                request.httpMethod = "POST"
                request.setValue("application/json", forHTTPHeaderField: "Content-Type")
                request.setValue("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6Miwicm9sZSI6InVzZXIiLCJpYXQiOjE1NDk0NjM1NzR9.X-d7mmVGPBA-_DzL36xBsTMhJP-Q0wId3MUoe_6SNv4", forHTTPHeaderField: "Authorization")
                let params = ["roomId": 2]
                
                guard let httpBody = try? JSONSerialization.data(withJSONObject: params, options: []) else {
                    return
                }
                request.httpBody = httpBody
                
                let session = URLSession.shared
                session.dataTask(with: request, completionHandler: { (data, response, error) in
                    print(String(data: data!, encoding: String.Encoding.utf8)!)
                    semaphore.signal()
                }).resume()
                semaphore.wait()
            }
        }
        zone.onContextChange = { contexts in
            print("Context change")
        }
    }
    
    fileprivate func updateLogs(_ log: String) {
        var logs = UserDefaults.standard.string(forKey: "logs")
        logs?.append("\(log)\n")
        UserDefaults.standard.set(logs, forKey: "logs")
        NotificationCenter.default.post(name: Notification.Name("log"), object: logs)
    }
    
    func startEstimotion(reason: EstimoteState) {
        if isValidTimeToStartTracking {
            if self.state == .notStarted &&
                (reason == .startFromForeground || reason == .startFromFrequentPlace) {
                // first launch of application
                self.state = reason
                startMonitoringTimer(600.0)
            }
            else if self.state == .notStarted && reason == .startFromPush {
                // start from push notification
                self.state = reason
                startMonitoringTimer(60.0)
            }
            
        }
        else {
            self.stopEstimotion()
        }
    }
    
    func stopEstimotion() {
        if self.state != .notStarted {
            self.state = .notStarted
            let app = UIApplication.shared
            var identifier = UIBackgroundTaskIdentifier.invalid
            identifier = app.beginBackgroundTask {
                app.endBackgroundTask(identifier)
                identifier = UIBackgroundTaskIdentifier.invalid
            }

            // Because, the updating often occurs in background mode,
            // we should do that by background task.
            if identifier != UIBackgroundTaskIdentifier.invalid {
                self.updateLogs("STOP ESTIMOTION")
                print("STOP ESTIMOTION")
                if self.proximityObserver != nil {
                    self.proximityObserver.stopObservingZones()
                    zone.onExit = nil
                    zone.onEnter = nil
                }
                // close background task
                app.endBackgroundTask(identifier)
            }
        }
    }
    
    func startMonitoringTimer(_ sec: Double) {
        let app = UIApplication.shared
        var identifier = UIBackgroundTaskIdentifier.invalid
        identifier = app.beginBackgroundTask {
            app.endBackgroundTask(identifier)
            identifier = UIBackgroundTaskIdentifier.invalid
        }

        // Because, the updating often occurs in background mode,
        // we should do that by background task.
        if identifier != UIBackgroundTaskIdentifier.invalid {
            observeInBackground()
            startTimerFor(sec)
//             Close the background task.
            app.endBackgroundTask(identifier)
        }
    }
    
    func observeInBackground() {
        let app = UIApplication.shared
        var identifier = UIBackgroundTaskIdentifier.invalid
        identifier = app.beginBackgroundTask {
            app.endBackgroundTask(identifier)
            identifier = UIBackgroundTaskIdentifier.invalid
        }

        // Because, the updating often occurs in background mode,
        // we should do that by background task.
        if identifier != UIBackgroundTaskIdentifier.invalid {
            print("START ESTIMOTION")
            setupEstimote()
            proximityObserver.startObserving([zone])
            // close background task
            app.endBackgroundTask(identifier)
        }
    }
    
    fileprivate func startTimerFor(_ sec: Double) {
        self.updateLogs("START TIMER FOR: \(sec)sec")
        print("📌START TIMER FOR: \(sec)")
        if isValidTimeToStartTracking {
            if timer != nil {
                timer!.pause()
            }
            timer = Repeater(interval: .seconds(sec), observer: { (timer) in
                self.timerTriggered()
            })
            timer?.start()
        }
    }
    
    @objc fileprivate func timerTriggered() {
        self.updateLogs("STOP TIMER")
        print("📌STOP TIMER")
        guard let existingTimer = timer else { return }
        existingTimer.pause()
        stopEstimotion()
    }
}
