//
//  ViewController.swift
//  PushEstimoteTest
//
//  Created by UDTech LLC on 4/22/19.
//  Copyright © 2019 UDTech LLC. All rights reserved.
//

import UIKit
import CoreLocation

class ViewController: UIViewController {
    
    let locationManager = CLLocationManager()
    static let geoCoder = CLGeocoder()

    @IBOutlet weak var logTextView: UITextView!
    fileprivate var logText = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        EstimotionManager.shared.startEstimotion(reason: .startFromForeground)
        NotificationCenter.default.addObserver(self, selector: #selector(logs(notification:)), name: Notification.Name("log"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(showLogs), name: UIApplication.didBecomeActiveNotification, object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
        super.viewWillDisappear(animated)
    }

    @objc func logs(notification: Notification) {
        if let text = notification.object as? String {
            DispatchQueue.main.async {
                self.logTextView.text = text
            }
        }
    }
    
    @objc func showLogs() {
        if let text = UserDefaults.standard.string(forKey: "logs") {
            DispatchQueue.main.async {
            self.logTextView.text = text
            }
        }
    }
}

extension ViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didVisit visit: CLVisit) {
        EstimotionManager.shared.startEstimotion(reason: .startFromFrequentPlace)
    }
}
